#include "stdafx.h"
#include "bitfield.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	cout << "Выделяем память под 5 бит" << endl;
	TBitField A(5);

	cout << "Устанавливаем бит 1" << endl;
	A.SetBit(0);
	cout << "Устанавливаем бит 2" << endl;
	A.SetBit(1);
	cout << "Устанавливаем бит 3" << endl;
	A.SetBit(2);
	cout << "Устанавливаем бит 4" << endl;
	A.SetBit(3);
	cout << "Устанавливаем бит 5" << endl;
	A.SetBit(4);

	cout << "Выводим битовое поле" << endl;
	for (int i = 0; i < A.GetLength(); i++) {
		cout << A.GetBit(i) << " ";
	}
	cout << endl;

	cout << "Очищаем бит 1" << endl;
	A.ClrBit(0);
	cout << "Очищаем бит 2" << endl;
	A.ClrBit(1);
	cout << "Очищаем бит 3" << endl;
	A.ClrBit(2);
	cout << "Очищаем бит 4" << endl;
	A.ClrBit(3);
	cout << "Очищаем бит 5" << endl;
	A.ClrBit(4);
	
	cout << "Выводим битовое поле" << endl;
	for (int i = 0; i < A.GetLength(); i++) {
		cout << A.GetBit(i) << " ";
	}
	cout << endl;

    return 0;
}